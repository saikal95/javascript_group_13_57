import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';
import { ServiceforUsers } from '../shared/serviceforus.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
})
export class NewUserComponent  {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput')  emailInput!: ElementRef;
  @ViewChild('booleanChoice') active!: ElementRef;
  @ViewChild('role')role!: ElementRef;
  meaning = false;

  constructor(public userServe: ServiceforUsers) {}

  createUser(){
    this.meaning = true;
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    let active = this.active.nativeElement.checked;
    const role = this.role.nativeElement.value;

    if (name === '' || email === '') {
      console.log('Insert all data');
    } if (active){
          this.meaning = true;
      } else {
        this.meaning= false;
      }


      const user = new User(name, email, this.meaning, role);
      this.userServe.usersArray.push(user);
    }


}
