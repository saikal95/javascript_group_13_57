import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { ServiceforUsers } from './shared/serviceforus.service';
import { FormsModule } from '@angular/forms';
import { GroupsComponent } from './groups/groups.component';
import { OneGroupComponent } from './groups/one-group/one-group.component';
import {GroupService} from "./shared/group.service";
import { NewGroupComponent } from './new-group/new-group.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UsersComponent,
    UserComponent,
    GroupsComponent,
    OneGroupComponent,
    NewGroupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ServiceforUsers, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
