import { Component, Input } from '@angular/core';
import { User } from '../../shared/user.model';
import { ServiceforUsers } from '../../shared/serviceforus.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],

})


export class UserComponent {
  @Input() oneUser!: User;

  activeCheck() {
    if (this.oneUser.active) {
      return 'yes';
    } else {
      return 'no'
    }
  }



}
