import {User} from "./user.model";
import {Group} from "./group.model";
import {EventEmitter} from "@angular/core";

export class GroupService {

  groupsChange= new EventEmitter<Group[]>();

 private groups: Group[] = [
   new Group('hing club'),
   new Group('dance club')
 ];


  getGroups() {
    return this.groups.slice();
  }

  addGroup(group:Group){
    this.groups.push(group);
    this.groupsChange.emit(this.groups);
  }
}
